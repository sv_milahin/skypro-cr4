from flask import Flask, jsonify
from flask_cors import CORS

from api.exceptions import BaseServiceError
from setup.api import api
from setup.db import db
from api.views import auth_ns, user_ns, genre_ns, director_ns, movie_ns, favorite_ns
from setup.jwt import jwt


def base_service_error_handler(exception):
    return jsonify({"error": str(exception)}), exception.code


def create_app(config_obj):
    app = Flask(__name__)
    app.config.from_object(config_obj)
    app.json.ensure_ascii = False

    CORS(app,
         resources={r"/*": {"origins": ["http://127.0.0.1:5000", "http://localhost:8080"]}},
         supports_credentials=True
         )

    db.init_app(app)
    api.init_app(app)
    jwt.init_app(app)

    api.add_namespace(user_ns)
    api.add_namespace(auth_ns)

    api.add_namespace(movie_ns)
    api.add_namespace(director_ns)
    api.add_namespace(genre_ns)
    api.add_namespace(favorite_ns)

    app.register_error_handler(BaseServiceError, base_service_error_handler)

    return app
