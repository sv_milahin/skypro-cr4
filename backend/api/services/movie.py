from api.exceptions import ItemNotFound


class MoviesService:
    def __init__(self, dao):
        self.dao = dao

    def get_item(self, pk):
        if movie := self.dao.get_by_id(pk):
            return movie
        raise ItemNotFound(f"Movie with pk={pk} not exists.")

    def get_all(self, page, status):
        return self.dao.get_all(page=page, status=status)
