from api.exceptions import ItemNotFound
from werkzeug.security import generate_password_hash, check_password_hash

class UserService:
    def __init__(self, dao):
        self.dao = dao

    def get_by_email(self, email):
        if user := self.dao.get_by_email(email):
            return user
        raise ItemNotFound(f"User with email '{email}' not exists.")

    def create(self, user_data):
        if self.dao.get_by_email(user_data.get("email")):
            return None
        user_data['password'] = generate_password_hash(user_data['password'])
        return self.dao.create(user_data)

    def patch(self, user_data):
        email = user_data.get("email")
        user = self.get_by_email(email)

        if 'name' in user_data:
            user.name = user_data.get("name")
        if 'surname' in user_data:
            user.surname = user_data.get("surname")
        if 'favourite_genre' in user_data:
            user.favourite_genre = int(user_data.get("favourite_genre"))

        self.dao.update(user)

    def update(self, user_data):
        email = user_data.get("email")
        user = self.get_by_email(email)
        if check_password_hash(user.password, user_data["password_1"]):
            user.password = generate_password_hash(user_data["password_2"])
            self.dao.update(user)

    def get_favorites(self, email):
        if user := self.dao.get_by_email(email):
            uid = user.id
            return self.dao.get_favorites(uid)
        return ""

    def add_favorire(self, email, mid):
        if user := self.dao.get_by_email(email):
            uid = user.id
            self.dao.add_favorite(uid, mid)

    def delete_favorite(self, email, mid):
        if user := self.dao.get_by_email(email):
            uid = user.id
            self.dao.delete_favorite(uid, mid)
