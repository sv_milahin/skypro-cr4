from api.exceptions import ItemNotFound


class GenresService:
    def __init__(self, dao):
        self.dao = dao

    def get_item(self, pk):
        if genre := self.dao.get_by_id(pk):
            return genre
        raise ItemNotFound(f"Genre with pk={pk} not exists.")

    def get_all(self, page, status=None):
        return self.dao.get_all(page=page, status=status)
