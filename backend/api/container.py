from api.dao.movie import MoviesDAO
from api.dao.director import DirectorDAO
from api.dao.genre import GenreDAO
from api.dao.user import UserDAO

from api.services.movie import MoviesService
from api.services.director import DirectorsService
from api.services.genre import GenresService
from api.services.user import UserService

from setup.db import db


movie_dao = MoviesDAO(db.session)
director_dao = DirectorDAO(db.session)
genre_dao = GenreDAO(db.session)
user_dao = UserDAO(db.session)


movie_service = MoviesService(dao=movie_dao)
director_service = DirectorsService(dao=director_dao)
genre_service = GenresService(dao=genre_dao)
user_service = UserService(dao=user_dao)
