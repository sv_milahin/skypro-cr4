from http import HTTPStatus

from flask import request
from flask_restx import Resource, Namespace

from flask_jwt_extended import jwt_required, get_jwt_identity

from setup.api.models import user_model
from api.container import user_service

api = Namespace("user")


@api.route("/")
class UserView(Resource):
    @api.expect(user_model)
    @api.marshal_with(user_model)
    @api.doc(description="Get user profile")
    @jwt_required()
    def get(self):
        """
        Get user's information
        """
        email = get_jwt_identity()
        return user_service.get_by_email(email)

    @api.doc(
        description="Change user data (name, surname, favorite genre).",
    )
    @jwt_required()
    def patch(self):
        """
        Change user data (name, surname, favorite genre).
        """
        req_json = request.json
        req_json["email"] = get_jwt_identity()
        user_service.patch(req_json)
        return "", HTTPStatus.NO_CONTENT


@api.route("/password/")
class PasswordView(Resource):

    @api.doc(
        description="Update user password",
    )
    @jwt_required()
    def put(self):
        """
        Update user password
        """
        req_json = request.json
        req_json["email"] = get_jwt_identity()
        user_service.update(req_json)
        return "", HTTPStatus.NO_CONTENT
