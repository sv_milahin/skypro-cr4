from .auth import api as auth_ns
from .user import api as user_ns
from .movie import api as movie_ns
from .genre import api as genre_ns
from .director import api as director_ns
from .favorite import api as favorite_ns

__all__ = [
    "auth_ns",
    "user_ns",
    "movie_ns",
    "genre_ns",
    "director_ns",
    "favorite_ns",

]