from http import HTTPStatus

from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restx import Namespace, Resource

from api.container import user_service

from setup.api.models import movie_model

api = Namespace("favorites")


@api.route("/movies")
class FavoritesView(Resource):


    @api.marshal_with(movie_model)
    @api.doc(description="Get favorites movies")
    @jwt_required()
    def get(self):
        """
        Get favorites movies
        """
        email = get_jwt_identity()
        print(email)
        movies = user_service.get_favorites(email)
        return movies, HTTPStatus.OK


@api.route("/movies/<int:mid>")
class FavoriteView(Resource):

    @api.doc(
            description="Add movie to favorite",
            params={"mid": "Movie ID"}
    )
    @jwt_required()
    def post(self, mid):
        """
        Add movie to favorites
        """
        email = get_jwt_identity()
        user_service.add_favorire(email, mid)
        return '', HTTPStatus.NO_CONTENT


    @api.doc(
            description="Delete movie from favorites",
            params={"mid": "Movie ID"}
    )
    @jwt_required()
    def delete(self, mid):
        """
        Delete movie from favorites
        """
        email = get_jwt_identity()
        user_service.delete_favorite(email, mid)
        return "", HTTPStatus.NO_CONTENT
