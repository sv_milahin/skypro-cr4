from http import HTTPStatus

from flask import request, abort
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_required)

from flask_restx import Resource, Namespace
from werkzeug.security import check_password_hash


from api.container import user_service
from setup.api.models import user_model, auth_model, login_model

api = Namespace("auth")


@api.route("/register")
class RegisterView(Resource):
    @api.expect(auth_model)
    @api.marshal_with(user_model)
    @api.doc(description="Register user")
    def post(self):
        """
        Create new user account
        """
        req = request.get_json()
        
        user = user_service.create(req)
        return user, HTTPStatus.CREATED


@api.route('/login')
class LoginView(Resource):
    @api.expect(auth_model)
    @api.marshal_with(login_model)
    @api.doc(description="SingUP user")
    def post(self):
        """
        Generate a JWT
        """

        req = request.get_json()

        email = req.get('email')
        password = req.get('password')

        user = user_service.get_by_email(email)

        if (user is not None) and check_password_hash(user.password, password):
            access_token = create_access_token(identity=user.email)
            refresh_token = create_refresh_token(identity=user.email)

            res = {
                'access_token': access_token,
                'refresh_token': refresh_token,
            }
            return res, HTTPStatus.CREATED
        return abort(HTTPStatus.BAD_REQUEST)


@api.route('/refresh')
class RefreshView(Resource):
    @jwt_required(refresh=True)
    @api.doc(description="Refresh JWT")
    def post(self):
        """
        Generate a new user JWT
        """
        email = get_jwt_identity()
        print(email)
        access_token = create_access_token(identity=email)
        return {'access_token': access_token}, HTTPStatus.CREATED
