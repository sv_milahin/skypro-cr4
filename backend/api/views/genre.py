from http import HTTPStatus

from flask_restx import Namespace, Resource

from api.container import genre_service
from setup.api.models import genre_model
from setup.api.parsers import page_parser

api = Namespace("genres")


@api.route("/")
class GenresView(Resource):
    @api.expect(page_parser)
    @api.marshal_with(genre_model)
    @api.doc(description="Get all genres",
             params={
                 "page": "Page",
                 "status": "Status",
             }
             )
    def get(self):
        """
        Get all genres
        """
        return genre_service.get_all(**page_parser.parse_args())


@api.route("/<int:genre_id>/")
class GenreView(Resource):
    @api.response(HTTPStatus.NOT_FOUND, "Not Found")
    @api.marshal_with(genre_model)
    @api.doc(
        description="Get genre by id",
        params={
            "genre_id": "Genre ID"
        }

    )
    def get(self, genre_id):
        """
        Get genre by id
        """
        return genre_service.get_item(genre_id)
