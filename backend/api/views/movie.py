from http import HTTPStatus

from flask_restx import Namespace, Resource

from api.container import movie_service
from setup.api.models import movie_model
from setup.api.parsers import page_parser

api = Namespace("movies")


@api.route("/")
class MoviesView(Resource):
    @api.expect(page_parser)
    @api.marshal_with(movie_model)
    @api.doc(description="Get all movies",
             params={
                 "page": "Page",
                 "status": "Status",
             }
             )
    def get(self):
        """
        Get all movies
        """
        return movie_service.get_all(**page_parser.parse_args())


@api.route("/<int:movie_id>/")
class MovieView(Resource):

    @api.marshal_with(movie_model)
    @api.doc(
        description="Get movie by id",
        params={
            "movie_id": "Movie ID"
        }

    )
    def get(self, movie_id):
        """
        Get movie by id
        """
        return movie_service.get_item(movie_id)
