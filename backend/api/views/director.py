from flask_restx import Namespace, Resource

from api.container import director_service
from setup.api.models import director_model
from setup.api.parsers import page_parser

api = Namespace('directors')


@api.route('/')
class DirectorsView(Resource):
    @api.expect(page_parser)
    @api.marshal_with(director_model)
    @api.doc(description="Get all directors",
             params={
                 "page": "Page",
                 "status": "Status",
             }
             )
    def get(self):
        """
        Get all directors
        """
        return director_service.get_all(**page_parser.parse_args())


@api.route('/<int:director_id>/')
class DirectorView(Resource):
    @api.response(404, 'Not Found')
    @api.marshal_with(director_model)
    @api.doc(
        description="Get director by id",
        params={
            "director_id": "Director ID"
        }

    )
    def get(self, director_id):
        """
        Get director by id
        """
        return director_service.get_item(director_id)
