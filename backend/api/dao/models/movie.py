from sqlalchemy import Column, String, Integer, Float, ForeignKey
from sqlalchemy.orm import relationship

from api.dao.models.director import Director
from api.dao.models.genre import Genre
from setup.db import models


class Movie(models.Base):
    __tablename__ = 'movies'

    title = Column(String(100), unique=True, nullable=False)
    description = Column(String(5000))
    trailer = Column(String(200))
    year = Column(Integer())
    rating = Column(Float())
    genre_id = Column(Integer(), ForeignKey(Genre.id))
    genre = relationship(Genre)
    director_id = Column(Integer(), ForeignKey(Director.id))
    director = relationship(Director)
