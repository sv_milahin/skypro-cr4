from sqlalchemy import Column, String

from setup.db import models


class User(models.Base):
    __tablename__ = 'users'

    email = Column(String(200), unique=True, nullable=False)
    password = Column(String(200), nullable=False)
    name = Column(String(100))
    surname = Column(String(200))
    favourite_genre = Column(String(200))
