from .base import BaseDAO
from .models.genre import Genre


class GenreDAO(BaseDAO):
    __model__ = Genre

