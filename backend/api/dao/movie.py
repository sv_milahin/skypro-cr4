from api.dao.base import BaseDAO
from api.dao.models.movie import Movie


class MoviesDAO(BaseDAO):
    __model__ = Movie
