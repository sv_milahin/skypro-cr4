from api.dao.base import BaseDAO
from api.dao.models.director import Director


class DirectorDAO(BaseDAO):
    __model__ = Director
