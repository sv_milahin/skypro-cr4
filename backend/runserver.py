from api.dao.models.movie import Movie
from api.dao.models.director import Director
from api.dao.models.genre import Genre
from api.dao.models.user import User
from api import create_app, db

from config import conf

app = create_app(conf)


@app.shell_context_processor
def shell():
    return {
        "db": db,
        "Movie": Movie,
        "Director": Director,
        "Genre": Genre,
        "User": User
    }
