from flask_restx import Api


api = Api(
    authorizations={
        "Bearer": {
            "type": "apiKey",
            "in": "header",
            "name": "Authorization",
            "description": "Add a JWT with ** Bearer &lt;JWT&gt; to authorize"
        }
    },
    title="Flask Course Project 4",
    description="Movies catalog REST API",
    doc="/docs",
    security="Bearer Auth"
)
