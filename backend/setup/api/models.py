from flask_restx import fields

from setup.api import api


director_model = api.model("Режиссёр", {
    "id": fields.Integer(required=True, example=1),
    "name": fields.String(required=True, max_length=100, example="Квентин Тарантино"),
})

genre_model = api.model("Жанр", {
    "id": fields.Integer(required=True, example=1),
    "name": fields.String(required=True, max_length=100, example="Комедия"),
})

movie_model = api.model("Фильм", {
    "id": fields.Integer(required=True, example=1),
    "title": fields.String(required=True, max_length=100, example="Название фильма"),
    "description": fields.String(max_length=5000, example="Описание фильма"),
    "trailer": fields.String(max_length=200, example="Комедия"),
    "year": fields.Integer(example=2022),
    "rating": fields.Float(example=4.8),
    "genre_id": fields.Integer(example=1),
    "director_id": fields.Integer(example=2),
    'genre': fields.Nested(genre_model),
    'director': fields.Nested(director_model)
})


user_model = api.model("Пользователь", {
    "id": fields.Integer(required=True, example=1),
    "email": fields.String(required=True, max_length=200),
    "password": fields.String(required=True, max_length=200),
    "name": fields.String(max_length=100),
    "surname": fields.String(max_length=200),
    "favourite_genre": fields.Integer(example=1)
})


auth_model = api.model("Авторизация", {
    "email": fields.String(required=True, max_length=100, example="user@email.me"),
    "password": fields.String(required=True, max_length=100, example="qwerty"),
})

login_model = api.model("Регистрация", {
    "access_token": fields.String(required=True),
    "refresh_token": fields.String(required=True),
})