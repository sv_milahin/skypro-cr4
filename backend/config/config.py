from datetime import timedelta
from pathlib import Path

from decouple import config

BASE_DIR = Path(__file__).resolve().parent.parent

class Config:
    SECRET_KEY = config("SECRET_KEY", "secret001")

    JWT_SECRET_KEY = config("JWT_SECRET_KEY", "secret002")
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=30)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=90)

    ITEMS_PER_PAGE = 12

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    RESTX_JSON = {
        "ensure_ascii": False,
    }


class DevConfig(Config):
    SQLALCHEMY_DATABASE_URI = f"sqlite:///{BASE_DIR}/db/dbase.db"
    SQLALCHEMY_ECHO = True


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = f"sqlite:///{BASE_DIR}/db/test.db"
    SQLALCHEMY_ECHO = True


class ProdConfig(Config):
    DEBUG = False


class ConfigFactory:
    flask_env = config('FLASK_ENV')

    @classmethod
    def get_config(cls):
        if cls.flask_env == 'development':
            return DevConfig
        elif cls.flask_env == 'production':
            return ProdConfig
        elif cls.flask_env == 'testing':
            return TestConfig
        raise NotImplementedError


conf = ConfigFactory.get_config()
